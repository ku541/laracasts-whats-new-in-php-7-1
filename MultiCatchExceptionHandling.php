<?php

class ChargeRejected extends Exception
{

}

class NotEnoughFunds extends Exception
{

}

class User
{
    public function subscribe()
    {
        throw new NotEnoughFunds;
    }
}

function flash($message)
{
    var_dump($message);
}

try {
    (new User)->subscribe();
} 
// catch (ChargeRejected $exception) {
//     flash('Failed');
// } catch (NotEnoughFunds $exception) {
//     flash('Failed');
// }
catch (ChargeRejected | NotEnoughFunds $exception) {
    flash('Failed');
}