<?php

$book = ['Book Name', 'Author Name'];
$book2 = ['book' => 'Book 2 Name', 'author' => 'Author 2 Name'];
$books = [
    ['book' => 'Book Name', 'author' => 'Author Name'],
    ['book' => 'Book 2 Name', 'author' => 'Author 2 Name']
];

// list($title, $author) = $book;
[$title, $author] = $book;
['book' => $title2, 'author' => $author2] = $book2;

var_dump($title, $author);
var_dump($title2, $author2);
foreach ($books as ['book' => $title, 'author' => $author]) {
    var_dump($title, $author);
}