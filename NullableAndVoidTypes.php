<?php

class User
{
    protected $age;

    // public function __construct($age)
    // {
    //     $this->age = $age;
    // }

    public function age() : ?int
    {
        return $this->age;
    }

    public function subscribe(?callable $callback = null) : void
    {
        var_dump('Subscribing Here');
        
        if ($callback) $callback();
    }
}

$age = (new User)->age();

(new User)->subscribe(function () {
    var_dump('Respond Here');
});

var_dump($age);